﻿using UnityEngine;
using System.Collections;

public class dayNight : MonoBehaviour {
	public Vector3 t;

	public bool spawnedOnce = false;
	public GameObject ghouls;
//	public float numGhouls;
	public bool isDay = true;
	public GameObject ghoulPrefab;
	public Transform head;

	public float timeSpeed = 5f;

	// Use this for initialization
	void Start () {
		Debug.Log ("Is day? " + isDay);
		
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (new Vector3 (timeSpeed * Time.deltaTime, 0f, 0f));

		isDay = (this.transform.rotation.eulerAngles.x > 0 && this.transform.rotation.eulerAngles.x < 90);
//		numGhouls = ghouls.transform.childCount;

		//if suns out
		if (isDay) {
			//hide ghosts
			ghouls.SetActive(false);
			spawnedOnce = false;
//			//if all ghosts are dead
//			if (ghouls.transform.childCount == 0) {
				//if haven't spawned ghosts yet
//				ghouls.GetComponentInChildren<Transform>().gameObject
//			}

		} else {
			if (!spawnedOnce) {
				Debug.Log ("Is day? " + isDay);
				float posNeg1 = Random.Range (0, 2);
				float posNeg2 = Random.Range (0, 2);
				Debug.Log (posNeg1);
				Debug.Log(posNeg2);
				if (posNeg1 == 0) {
					posNeg1 = -1;
				}
				if (posNeg2 == 0) {
					posNeg2 = -1;
				}
				ghouls.transform.position = head.transform.position + new Vector3 (posNeg1 * Random.Range (7, 15), Random.Range (0, 7), posNeg2 * Random.Range (7, 15));
				ghouls.SetActive(true);
//				Debug.Log ("ghosts appear");
				//spawn 9 ghosts
//				Debug.Log("creating ghosts");
				while (ghouls.transform.childCount < 10) {
					Debug.Log ("child count is now " + ghouls.transform.childCount);
					GameObject ghoul = Instantiate (ghoulPrefab) as GameObject;
					ghoul.transform.position = ghouls.transform.position;
					ghoul.transform.SetParent(ghouls.transform);
					ghoul.transform.localPosition = new Vector3 (Random.Range (-9, 9), Random.Range (1, 5), Random.Range (-9, 9));
					ghoul.GetComponent<BoidBrain> ().player = head;
				}
				spawnedOnce = true;
			}
			if (ghouls.transform.childCount < 2) {
				ghouls.SetActive(false);
			}
		}


	
	}
}
