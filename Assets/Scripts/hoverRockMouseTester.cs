﻿using UnityEngine;
using System.Collections;

public class hoverRockMouseTester: MonoBehaviour {

	private GameObject obj;

	public GameObject hoverRockPrefab;

	public Transform trans;

	public Transform reticle;
	private bool isAiming = true;
	public float maxDistance = Mathf.Infinity;
	private Texture thingTexture;

	public Transform camTrans;

	public float moveSpeed = 1f;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {

		if (isAiming)
		{

			// Choose target area
			RaycastHit hit;
			bool foundHit;

			foundHit = Physics.Raycast(camTrans.transform.position, camTrans.transform.forward, out hit, maxDistance);
			//Debug.DrawRay(transform.position, transform.forward, Color.cyan, 10f);


			// TODO: Add checking for valid spots

			if (foundHit)
			{
				 Debug.Log("Found hit");

				// position reticle
				reticle.position = hit.point;
				// reticle.rotation = Quaternion.LookRotation(hit.normal);
				reticle.rotation = Quaternion.LookRotation(hit.normal) * Quaternion.Euler(90, 0, 0);
				reticle.gameObject.SetActive(true);

				// retrieve texture from spot
//				thingTexture = hit.collider.gameObject.GetComponent<Renderer>().material.GetTexture("_MainTex");
			}
		}

//		trans = UnityEditor.SceneView.lastActiveSceneView.camera.transform;
		if (Input.GetMouseButtonDown(0))
		{
			obj = Instantiate (hoverRockPrefab) as GameObject;
			obj.transform.position = new Vector3 (transform.position.x, 0, transform.position.z);
//			obj.GetComponent<HoverRock> ().setPlayer(trans);

		}			

		if (Input.GetMouseButtonDown (1)) {
			obj.GetComponent<HoverRock> ().setWantsToGetOff(true);
		}

		var d = Input.GetAxis("Mouse ScrollWheel");
		if (d > 0f)
		{
			// scroll up
			obj.GetComponent<HoverRock> ().setMove(true);
		}
		else if (d < 0f)
		{
			// scroll down
			obj.GetComponent<HoverRock> ().setMove(false);
		}

		if (Input.GetMouseButtonDown(2))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("hoverRockTest");
		}
	
	}
}
