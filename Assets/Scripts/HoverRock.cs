﻿using UnityEngine;
using System.Collections;

public class HoverRock : MonoBehaviour {

	public float moveSpeed = .5f;
	public float moveDist = 10f;
	public float raiseHeight = .2f;
	public float raiseSpeed = .1f;
	private Vector3 moveDir; //default should be set to where player is looking
//	private float curDist = 0f;

    public bool moving = false;

    private bool hasBeenInstantiated;
	public GameObject miniSpurtPrefab;
	private GameObject spurtObj;

//	private GameObject hoverRockPrefab;
	private Rigidbody hoverRockRb;
	private GameObject hoverRock;

	public Transform player;
	public Transform reticle;
	public Transform playerSpace;
//	public Transform controller;
	//public Transform moveTarget;

	public bool creating = true;
	public bool wantsToGetOff = false;

	public bool createdSinkSpurt = false;
	//what button will toggle hoverRock?
	public float timeCreated;

	//maybe control direction with controller? pointed at ground directly in front of where want to go


	// Use this for initialization
	void Start () {
		hoverRock = transform.GetChild(0).gameObject;
//		hoverRock.transform.rotation = transform.rotation;

		hoverRockRb = hoverRock.GetComponent<Rigidbody>();
//		hoverRock.gameObject.transform.SetParent(player.transform);
//		reticle = controller.GetComponent<BendController>().reticle;
//		reticle = player.GetComponent<hoverRockMouseTester>().reticle;


		hoverRockRb.isKinematic = true;

		moving = false;
		//create hoverRock below player and raise up

		//set direction

		//spurt
		timeCreated = Time.time;



	
	}
	
	// Update is called once per frame
	void Update () {

		hoverRock.transform.rotation = Quaternion.LookRotation (Vector3.Cross(player.right, Vector3.up)); //Quaternion.RotateTowards (hoverRock.transform.rotation, Quaternion.LookRotation(player.forward,Vector3.up), 400f * Time.deltaTime);
		//float up from ground
		if (hoverRock.transform.position.y < raiseHeight && creating) {
			createMiniSpurt ();
			hoverRock.transform.position += new Vector3 (0, raiseSpeed, 0);

		} else { //start doing hoverRock things
			hoverRock.transform.position = new Vector3 (player.position.x, hoverRock.transform.position.y, player.position.z);
			creating = false;
			//hoverRockRb.isKinematic = false;
			hoverRockRb.useGravity = false;
			//toggle move
			if (moving && reticle.gameObject.activeSelf) {
//				Debug.Log ("DID THE THING");
//				//create spurts
//				if (!hasBeenInstantiated) {
//					spurtObj = Instantiate (miniSpurtPrefab) as GameObject;
//					spurtObj.transform.position = new Vector3 (hoverRock.transform.position.x, 0, hoverRock.transform.position.z);
//					hasBeenInstantiated = true;
//					spurtObj.GetComponent<ParticleSystem> ().loop = true;
//				}
				//set destination to be reticle position with same elevation as player
				Vector3 playerSpaceDest = new Vector3 (reticle.position.x, playerSpace.position.y, reticle.position.z);
				Vector3 boardDest = new Vector3 (reticle.position.x, hoverRock.transform.position.y, reticle.position.z);

				//move player
				playerSpace.position = Vector3.Lerp(playerSpace.transform.position, playerSpaceDest, Time.deltaTime*moveSpeed);
				hoverRock.transform.position = Vector3.Lerp(hoverRock.transform.position, boardDest, Time.deltaTime*moveSpeed);
//				if (hoverRock.transform.position.x == reticle.position.x && hoverRock.transform.position.z == reticle.position.z) {
//					Debug.Log ("reticle and hoverrock at same spot");
//					moving = false;
//					wantsToGetOff = true;
//				}
				//			if (wantsToGetOff) {
				//				sinkIntoGround ();
				//			}
			}
		}
//		Debug.Log ("wants to get off? " + wantsToGetOff);
		if (wantsToGetOff || Time.time - timeCreated >= 5f) {
			moving = false;
			hasBeenInstantiated = false;
			sinkIntoGround ();
		}
			



		//calculate curDist
		//if dist not reached


		//move player in direction

		//spurt

		//sink back into ground when dist reached or if user wants to get off



		//destroy afterwards
//		Destroy (miniSpurtObj, 5);
//		Destroy (transform.gameObject, 7);
	
	}

	void sinkIntoGround() {
//		Debug.Log ("Sinking into ground");
		//sink
		if (hoverRock.transform.position.y > -1) {
			hoverRock.transform.position -= new Vector3 (0, raiseSpeed, 0);
		} else {
			//destroy
			//		Debug.Log("destroyed");
			if (!createdSinkSpurt) {
				createMiniSpurt ();
				createdSinkSpurt = true;
			}

//			Destroy(transform.gameObject);
//			Destroy (hoverRock);
//			Destroy (spurtObj);
		}
	}

	void createMiniSpurt() {
		if (!hasBeenInstantiated) {
			GameObject miniSpurtObj = Instantiate (miniSpurtPrefab) as GameObject;
			miniSpurtObj.transform.position = new Vector3 (hoverRock.transform.position.x, 0, hoverRock.transform.position.z);
			Destroy (miniSpurtObj, 3);
			hasBeenInstantiated = true;
		}
	}

//	public void setPlayer(Transform trans) {
//		player = trans;
//	}

//	public void setController(Transform trans) {
//		controller = trans;
//	}

//	public void setReticle(Transform trans) {
//		reticle = trans;
//	}

	public void setMove(bool m) {
		moving = m;
	}

	public void setWantsToGetOff(bool b) {
		wantsToGetOff = b;
	}

//	public Transform setMoveTarget(Transform t) {
//		moveTarget = t;
//	}

}
