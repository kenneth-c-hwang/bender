﻿using UnityEngine;
using System.Collections;

public class buildController : MonoBehaviour {

	private SteamVR_TrackedObject trackedObj;
	private SteamVR_Controller.Device device;
	private int deviceIdx;

	public GameObject block;

	private bool placingBlock;

	// Use this for initialization
	void Start () {
		deviceIdx = (int)trackedObj.index;
		device = SteamVR_Controller.Input(deviceIdx);
	}
	
	// Update is called once per frame
	void Update () {
		//set reticle to transparent building block



		if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
		{
			placingBlock = true;
			//make block reticle non-transparent
//			block = Instantiate (rockPrefab) as GameObject;

			GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			go.transform.position = transform.position;

		}
		if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger)) {
			placingBlock = false;
		}
//		if (device.GetAxis(SteamVR_Controller.ButtonMask.Touchpad))
//		{
//			if (placingBlock) {
//				//if trigger held, scale block based on axis
//			}
//			//choose which block to use
//
//		}
	}

	void OnTriggerStay(Collider col)
	{
		//Grab object
		if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
		{
			col.gameObject.GetComponent<Rigidbody>().isKinematic = true;
			col.gameObject.transform.SetParent(gameObject.transform);
		}
		if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
		{
			col.attachedRigidbody.isKinematic = false;
			col.gameObject.transform.SetParent(null);
		}
	}

}
