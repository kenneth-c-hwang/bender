﻿using UnityEngine;
using System.Collections;

public class summonRock : MonoBehaviour {

    public GameObject miniSpurtPrefab;
    public GameObject spurtPrefab;
    private GameObject rockPrefab;

    //public float rockHeight;
    public float raiseHeight = 1f;
    public float floatRate = .05f;
    private Rigidbody rockRb;
    private GameObject rock;

//    private bool spurting = true;
    public bool creating = true;

    public float forcePower = 3000f;

	public float rotationRate = 15f;

	public Transform controllerTransform;

	private bool rotated;
	public bool shouldRotate;

	public bool shot;

//	public float timeRockCreated;


    // Use this for initialization
    void Start () {
//		timeRockCreated = Time.time;
        GameObject miniSpurtObj = Instantiate(miniSpurtPrefab) as GameObject;
        miniSpurtObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);

        rock = transform.GetChild(0).gameObject;
        //rockHeight = rock.GetComponent<Renderer>().bounds.size.y;
        //Debug.Log(rockHeight);

        //rock.transform.position = new Vector3(transform.position.x, -rockHeight*.8f, transform.position.z);
        rock.transform.rotation = transform.rotation;

        rockRb = rock.GetComponent<Rigidbody>();
        rockRb.isKinematic = true;

		Destroy (miniSpurtObj, 5);
		Destroy (transform.gameObject, 7);

    }



    // Update is called once per frame
    void Update () {
		if (rock.transform.position.y < raiseHeight && creating) {
			floatUp ();
		} else {
			if (shouldRotate) {
				rotateForward ();
			}
//			rotateForward ();
//			StartCoroutine(Example());
		}
    }

//	IEnumerator Example() {
//		bool waited = false;
//		if (!waited) {
//			yield return new WaitForSeconds (1);
//			waited = true;
//		} else {
//			rotateForward ();
//		}
//	}

	void floatUp(){
		rock.transform.position += new Vector3(0, floatRate, 0);
		if (rock.transform.position.y >= raiseHeight)
		{
			creating = false;
			rockRb.isKinematic = false;
			rockRb.useGravity = false;
		}
	}

	void rotateForward() {
		rotationRate = 250f;
		Vector3 forward = controllerTransform.forward;
		Vector3 right = controllerTransform.right;
//		rock.transform.rotation =  Quaternion.Slerp (rock.transform.rotation, Quaternion.LookRotation(Vector3.Cross(forward,right), forward), rotationRate * Time.deltaTime);
		//Quaternion.LookRotation(Vector3.Cross(forward,right), forward);


		rock.transform.rotation =  Quaternion.RotateTowards (rock.transform.rotation, Quaternion.LookRotation(Vector3.Cross(forward,right), forward), rotationRate * Time.deltaTime);
//
//		float t = (Time.time - startTime) / duration;
//		transform.position = new Vector3(Mathf.SmoothStep(minimum, maximum, t), 0, 0);

		rotated = true;
	}

	public void setControllerTransform(Transform trans) {
		controllerTransform = trans;
	}

	public void triggerRotate(bool rot) {
		if (!shot) {
			shouldRotate = rot;
		}
	}
    

	public void Shoot()
    {
		if (rotated) {
			rockRb.AddForce (forcePower * controllerTransform.forward);
			rockRb.useGravity = true;
			shouldRotate = false; //prevents spike from reorienting to camera forward after shooting
			//still need to watch out for button mapped to triggerRotate method:
			// setting it to true again will cause shot spikes to reorient
			shot = true;
		}
    }

}
