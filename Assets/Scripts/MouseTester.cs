﻿using UnityEngine;
using System.Collections;

public class MouseTester : MonoBehaviour {

    public GameObject rockPrefab;
    public GameObject spikePrefab;
    public GameObject wallPrefab;

	private GameObject obj;

	private Transform trans;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		trans = UnityEditor.SceneView.lastActiveSceneView.camera.transform;
        if (Input.GetMouseButtonDown(0))
        {
            //GameObject rockObj = Instantiate(rockPrefab) as GameObject;
            //rockObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);

//			obj = Instantiate(spikePrefab) as GameObject;
//			obj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
//			obj.GetComponent<summonRock>().setControllerTransform(trans);

//            GameObject wallObj = Instantiate(wallPrefab) as GameObject;
//            wallObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
//			createSpike ();
        }

		//UnityEditor.SceneView.lastActiveSceneView.camera.transform

//		if (obj && obj.activeSelf && Input.GetMouseButtonDown(1))
//		{
//			obj.GetComponent<summonRock>().Shoot();
//			//obj.GetComponent<summonRock>().rotateForward();
//		}

		if (Input.GetMouseButtonDown (1)) {
			if (obj && obj.activeSelf) {
				obj.GetComponent<summonRock> ().triggerRotate (true);
			}
		}

//		obj.GetComponent<summonRock> ().triggerRotate (Input.GetMouseButtonDown (1));

		var d = Input.GetAxis("Mouse ScrollWheel");
		if (d > 0f)
		{
			// scroll up
			shootSpike();
		}
		else if (d < 0f)
		{
			// scroll down
			createSpike ();
		}

        if (Input.GetMouseButtonDown(2))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
        }
    }

	void createSpike() {
		obj = Instantiate (spikePrefab) as GameObject;
		obj.transform.position = new Vector3 (transform.position.x, 0, transform.position.z);
		obj.GetComponent<summonRock> ().setControllerTransform (trans);
//		obj.GetComponent<summonRock> ().triggerRotate (Input.GetMouseButtonDown (1));
	}

	void shootSpike() {
		if (obj && obj.activeSelf) {
			obj.GetComponent<summonRock> ().Shoot ();
		}
	}

//	void rotateSpike() {
//		if (obj && obj.activeSelf) {
//			obj.GetComponent<summonRock> ().rotateForward ();
//		}
//	}
		
}
